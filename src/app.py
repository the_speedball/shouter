import json
from flask import Flask, request, make_response
app = Flask(__name__)


@app.route('/')
def shout():
    rsp = {k: v.upper() for k, v in request.args.items()}
    return make_response(json.dumps(rsp))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
