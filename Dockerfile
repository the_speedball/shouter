FROM python:3.6

COPY . /srv
WORKDIR /srv

RUN pip install uwsgi
RUN pip install -r requirements.txt

CMD python src/app.py